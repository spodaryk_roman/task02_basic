package ua.lviv.iot;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * public class Main.
 *
 * @author Roman Spodaryk
 * @version 1.0
 */
public class Main {
    /**
     * the start of the interval.
     */
    private static int startOfTheInterval;
    /**
     * the end of the interval.
     */
    private static int endOfTheInterval;
    /**
     * initialization of the scanner class object.
     */
    private static Scanner scanner = new Scanner(System.in);

    /**
     * initialize all variables which are
     * used in printIntervalFromStartToEnd
     * and printIntervalFromEndToStart functions.
     */
    private static void init() {
        System.out.println("print start of the interval");
        try {
            startOfTheInterval = scanner.nextInt();

            System.out.println("print end of the interval");

            endOfTheInterval = scanner.nextInt();
            if (endOfTheInterval < startOfTheInterval) {
                throw new Exception();
            }
        } catch
        (InputMismatchException e) {
            System.out.println("you didn't write int");
        } catch (Exception e) {
            System.out.println("Exception : your endOfTheInterval is "
                    + "smaller then startOfTheInterval");
        }

    }

    /**
     * print all numbers in interval which are
     * selected by user from start to end.
     */
    private static void printIntervalFromStartToEnd() {
        System.out.println("printIntervalFromStartToEnd : ");
        for (int i = startOfTheInterval; i <= endOfTheInterval; i++) {
            System.out.println(i);
        }
        System.out.println();
    }

    /**
     * print all numbers in interval which are
     * selected by user from end to start.
     */
    private static void printIntervalFromEndToStart() {
        System.out.println("printIntervalFromEndToStart : ");
        for (int i = endOfTheInterval; i >= startOfTheInterval; i--) {
            System.out.println(i);
        }
        System.out.println();
    }

    /**
     * print sum of all numbers.
     */
    private static void sumOfAllNumbers() {
        int sum = 0;
        for (int i = endOfTheInterval; i >= startOfTheInterval; i--) {
            sum += i;
        }
        System.out.println("sumOfAllNumbers = " + sum);
        System.out.println();
    }

    /**
     * print sum of odd numbers.
     */
    private static void sumOfOddNumbers() {
        int sum = 0;
        for (int i = endOfTheInterval; i >= startOfTheInterval; i--) {
            if (i % 2 != 0) {
                sum += i;
            }
        }
        System.out.println("sumOfOddNumbers = " + sum);
        System.out.println();
    }


    /**
     * print sum of even numbers.
     */
    private static void sumOfEvenNumbers() {
        int sum = 0;
        for (int i = endOfTheInterval; i >= startOfTheInterval; i--) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        System.out.println("sumOfEvenNumbers = " + sum);
        System.out.println();
    }

    /**
     * print the first N numbers in row of Fibonacci numbers
     * N is selected by user
     * print the biggest odd and even numbers
     * print the percentages of odd and even numbers.
     */
    private static void printFibonacciNumbers() {
        int numberOfNumbersInRow = 0;
        try {
            System.out.println("enter the number of numbers in row: ");
            numberOfNumbersInRow = scanner.nextInt();
            if (numberOfNumbersInRow < 0) {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Exception : you enter the negative number");
        }

        List<Double> listOfFibonacciNumbers = new ArrayList<>();

        double theBiggestOddNumber;
        double countOfOddNumbers = 0;
        double theBiggestEvenNumber;
        double countOfEvenNumbers = 0;

        if (numberOfNumbersInRow == 0) {
            System.out.println("the row size is 0");
        } else if (numberOfNumbersInRow == 1) {
            listOfFibonacciNumbers.add(0.0);
            theBiggestEvenNumber = 0;
            countOfEvenNumbers++;
            System.out.println("The biggest even number (F2) is "
                    + theBiggestEvenNumber);
            System.out.println("Percentage  of even numbers is "
                    + (countOfEvenNumbers / listOfFibonacciNumbers.size()));
        } else if (numberOfNumbersInRow == 2) {
            listOfFibonacciNumbers.add(0.0);
            theBiggestEvenNumber = listOfFibonacciNumbers.get(0);
            countOfEvenNumbers++;
            listOfFibonacciNumbers.add(1.0);
            theBiggestOddNumber = listOfFibonacciNumbers.get(1);
            countOfOddNumbers++;
            System.out.println("The biggest odd number (F1) is "
                    + theBiggestOddNumber);
            System.out.println("The biggest even number (F2) is "
                    + theBiggestEvenNumber);
            System.out.println("Percentage  of even numbers is "
                    + (countOfEvenNumbers / listOfFibonacciNumbers.size()));
            System.out.println("Percentage  of odd numbers is "
                    + (countOfOddNumbers / listOfFibonacciNumbers.size()));
        } else {
            listOfFibonacciNumbers.add(0.0);
            theBiggestEvenNumber = listOfFibonacciNumbers.get(0);
            countOfEvenNumbers++;
            listOfFibonacciNumbers.add(1.0);
            theBiggestOddNumber = listOfFibonacciNumbers.get(1);
            countOfOddNumbers++;

            for (int i = 0; i < numberOfNumbersInRow - 2; i++) {
                double temp = listOfFibonacciNumbers.get(i)
                        + listOfFibonacciNumbers.get(i + 1);
                if (temp % 2 == 0) {
                    System.out.println("test = " + temp);
                    theBiggestEvenNumber = temp;
                    countOfEvenNumbers++;
                } else {
                    theBiggestOddNumber = temp;
                    countOfOddNumbers++;
                }
                listOfFibonacciNumbers.add(temp);
            }

            System.out.println("The first " + numberOfNumbersInRow
                    + " Fibonacci numbers are:");
            for (Double listOfFibonacciNumber : listOfFibonacciNumbers) {
                System.out.println(listOfFibonacciNumber);
            }

            System.out.println("The biggest odd number (F1) is "
                    + theBiggestOddNumber);
            System.out.println("The biggest even number (F2) is "
                    + theBiggestEvenNumber);
            System.out.println("Percentage of even numbers is "
                    + (countOfEvenNumbers / listOfFibonacciNumbers.size()));
            System.out.println("Percentage of odd numbers is "
                    + (countOfOddNumbers / listOfFibonacciNumbers.size()));
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        init();
        printIntervalFromStartToEnd();
        printIntervalFromEndToStart();
        sumOfAllNumbers();
        sumOfEvenNumbers();
        sumOfOddNumbers();
        printFibonacciNumbers();
    }
}
